package com.air.ui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
/**
 * 表格Swing
 * @author Air
 *
 */
public class ResultJTable extends JTable{
	/**
	 * 重写 实现 不同行不同颜色
	 */
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component pr = super.prepareRenderer(renderer, row, column);
		Color c = Color.WHITE;
		if(row % 2 == 0) {
			c = Color.GRAY;
		}
		pr.setBackground(c);
		return pr;
	}

}
