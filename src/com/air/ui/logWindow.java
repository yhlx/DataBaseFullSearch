package com.air.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class logWindow {

	private static JFrame frmLoginfo;
    private static JScrollPane scr1;
    /**
	 * Create the application.
	 */
	public logWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLoginfo = new JFrame();
		frmLoginfo.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frmLoginfo.setTitle("   loginfo");
		frmLoginfo.setBounds(0, 0, 800, 613);
		Log = new JTextArea();
		Log.setFont(new Font("Monospaced", Font.PLAIN, 14));
		  scr1 = new JScrollPane(Log);
		scr1.setBounds(0, 0, 780, 593);
		scr1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scr1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		JPanel pan = new JPanel();
		frmLoginfo.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
            	pan.setSize(frmLoginfo.getWidth(), frmLoginfo.getHeight());
            	scr1.setSize(frmLoginfo.getWidth()-33, frmLoginfo.getHeight()-39);
            	Log.setSize(scr1.getWidth(), scr1.getHeight());
	        }
        });
		pan.setLayout(null);
		pan.add(scr1);
		frmLoginfo.getContentPane().add(pan);
	}
	/**
	 * 追加一条信息  后 换行
	 * @param info
	 */
	public static final void log(String info) {
		if(null == Log) {
			logWindow window = new logWindow();
			logWindow.frmLoginfo.setVisible(true);
		}
		String text = info + "\n";
		if(Log.getText().length() > 80000){
            Log.setText(text);
		}else{
            Log.append(text);
        }

		JScrollBar jscrollBar = scr1.getVerticalScrollBar();
		if (jscrollBar != null) {
            jscrollBar.setValue(jscrollBar.getMaximum());
        }
	}
	/**
	 * 追加一条信息 不换行
	 * @param info
	 */
	public static final void logNotChangeLine(String info) {
		if(null == Log) {
			logWindow window = new logWindow();
			logWindow.frmLoginfo.setVisible(true);
		}
	}
	/**
	 * 清空窗口
	 */
	public static final void clearLogs() {
		logWindow.log("");
		Log.setText("");
	}
	/**
	 * 显示隐藏 窗口
	 * @param show
	 */
	public static final void setLogWindowVisualble (boolean show) {
		if(null != frmLoginfo) {
			frmLoginfo.setVisible(show);
		}
	}
	private static JTextArea Log= null;
}
