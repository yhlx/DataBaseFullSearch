package com.air.ui;

import com.air.db.DbConnectionFactory;
import com.air.tools.PropertiesTools;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

/**
 * 点击设置数据库连接 弹出的设置窗口
 * 
 * @date 2018年11月21日10:52:26
 * @author Air
 */
public class DataBaseConfig extends JDialog {

	private JPanel contentPanel = new JPanel();
    private JTextField textField_Url;
	private JTextField textField_User;
	private JTextField textField_Pass;
	private JComboBox<String> comboBox;
	private JLabel label_DbType;
    private JLabel label_DbUrl;
	private JLabel label_User;
	private JLabel label_Pass;
	private JPanel buttonPane;
	private JButton btn_TestDB;
	private JButton btn_Save;
	private JButton btn_Cancel;

	/**
	 * 默认构造方法
	 */
	public DataBaseConfig() {
		initUI();
		initListener();
	}

	/**
	 * 注册事件监听器
	 */
	private void initListener() {
	    btn_Cancel.addActionListener((e)->{
            DataBaseConfig.this.dispose();
        });
        btn_Save.addActionListener((e)->{
            PropertiesTools pro = PropertiesTools.getInstance();
            pro.saveDbPass(textField_Pass.getText());
            pro.saveDbUser(textField_User.getText());
            pro.saveDbUrl(textField_Url.getText());
            pro.saveDbType(String.valueOf(comboBox.getSelectedIndex()));
            DataBaseConfig.this.dispose();
        });
        btn_TestDB.addActionListener((e)->{
            PropertiesTools pro = PropertiesTools.getInstance();
            String driver = pro.getJdbcClass(comboBox.getSelectedIndex());
            if(null == DbConnectionFactory.getConnection(textField_Url.getText()
                    ,textField_User.getText() , textField_Pass.getText(),
                    driver)){
                JOptionPane.showMessageDialog(DataBaseConfig.this
                        , "数据库连接失败!", "错误:", JOptionPane.ERROR_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(DataBaseConfig.this
                        , "数据库连接成功!", "成功:", JOptionPane.INFORMATION_MESSAGE);
            }
        });
	}

	/**
	 * 初始化界面
	 */
	private void initUI() {
		setAlwaysOnTop(true);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("设置数据库连接");
		setBounds(100, 100, 730, 250);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
        PropertiesTools pro = PropertiesTools.getInstance();
		label_DbType = new JLabel("数据库类型:");
		label_DbType.setBounds(10, 21, 77, 15);
		contentPanel.add(label_DbType);

		comboBox = new JComboBox();
		comboBox.setBounds(122, 18, 561, 30);
		contentPanel.add(comboBox);

        label_User = new JLabel("数据库用户名:");
		label_User.setBounds(10, 58, 102, 15);
		contentPanel.add(label_User);

		label_Pass = new JLabel("数据库密码:");
		label_Pass.setBounds(10, 104, 77, 15);
		contentPanel.add(label_Pass);

        label_DbUrl = new JLabel("数据库地址:");
        label_DbUrl.setBounds(10, 144, 77, 15);
        contentPanel.add(label_DbUrl);

		textField_User = new JTextField();
		textField_User.setBounds(122, 55, 561, 30);
		contentPanel.add(textField_User);
		textField_User.setColumns(10);

		textField_Pass = new JTextField();
		textField_Pass.setColumns(10);
		textField_Pass.setBounds(122, 101, 561, 30);
        contentPanel.add(textField_Pass);

        textField_Url = new JTextField();
        textField_Url.setColumns(10);
        textField_Url.setBounds(122, 140, 561, 30);
		contentPanel.add(textField_Url);

		buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		btn_TestDB = new JButton("测试连接");
		buttonPane.add(btn_TestDB);

		btn_Save = new JButton("保存");
		btn_Save.setActionCommand("OK");
		buttonPane.add(btn_Save);
		getRootPane().setDefaultButton(btn_Save);

		btn_Cancel = new JButton("取消");
		btn_Cancel.setActionCommand("Cancel");
		buttonPane.add(btn_Cancel);

        List<String> dbTypeList = pro.getDbTypeList();
        dbTypeList.stream().forEach((str)-> comboBox.addItem(str)  );
        comboBox.setSelectedIndex(pro.getDbType());
        textField_Pass.setText(pro.getDbPass());
        textField_User.setText(pro.getDbUser());
        textField_Url.setText(pro.getDbUrl());
	}
}
