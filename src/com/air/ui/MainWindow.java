package com.air.ui;

import com.air.listeners.StartSearchListener;
import com.air.listeners.StopSearchListener;
import com.air.tools.PropertiesTools;
import com.air.tools.StringTools;
import com.air.tools.updateTable;
import lombok.Data;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;

/**
 * 程序主界面
 *
 * @author Air
 */
public class MainWindow extends JFrame {
    private static final long serialVersionUID = -4808435375272818952L;
    private JPanel contentPane;
    private JTextField textField_SearchText;
    private JTable table;
    private JButton btn_Start;
    private JButton btn_Stop;
    private JScrollPane scrollPane_Table;
    private JMenu menu_Config;
    private JMenuItem menuItem_Db;
    private JMenuItem menuItem_Thread;
    private JMenuItem menuItem_LikeType;
    private JMenuItem menuItem_About;
    private JMenuBar mainMeunBar;
    private String columTableName = "表名";
    private String columTableColumName = "字段名";
    private String columSql = "SQL语句";
    private String columRowNumber = "行号";
    private String titleStr = "数据库全局模糊搜索工具";
    private String[] tableTitles = null;
    private int tableW = 1080;

    /**
     * 根据输入内容开始搜索
     */
    public void updateTable() {
        updateTable.updateResult(this);
    }

    /**
     * 程序主入口
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                // 启用 BettyEyes 框架
                // 设置强透明
                BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.osLookAndFeelDecorated;
                BeautyEyeLNFHelper.launchBeautyEyeLNF();
                // 隐藏 标题栏 设置按钮
                UIManager.put("RootPane.setupButtonVisible", false);
                // 解决输入法白屏可能性
                System.setProperty("sun.java2d.noddraw", "true");
                UIManager.put("RootPane.setupButtonVisible", false);
                UIManager.put("TabbedPane.tabAreaInsets", new InsetsUIResource(0, 0, 0, 0));
                UIManager.put("TabbedPane.contentBorderInsets", new InsetsUIResource(0, 0, 2, 0));
                UIManager.put("TabbedPane.tabInsets", new InsetsUIResource(3, 10, 9, 10));
                Font frameTitleFont = (Font) UIManager.get("InternalFrame.titleFont");
                frameTitleFont = frameTitleFont.deriveFont(Font.PLAIN);
                UIManager.put("InternalFrame.titleFont", frameTitleFont);
                MainWindow frame = new MainWindow();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 默认构造方法
     */
    public MainWindow() {
        setTitle(titleStr);
        initUI();
        initListener();
    }

    /**
     *
     * 注册界面监听器
     */
    private void initListener() {
        menuItem_About.addActionListener((e) -> {
            JOptionPane.showMessageDialog(MainWindow.this
                    , "数据库表内容全局搜索工具 V1.1 By Air", "关于:", JOptionPane.INFORMATION_MESSAGE);
        });
        menuItem_Db.addActionListener((e) -> {
            new DataBaseConfig().setVisible(true);
        });
        menuItem_Thread.addActionListener((e) -> {
            PropertiesTools pro = PropertiesTools.getInstance();
            String numStr = JOptionPane.showInputDialog(MainWindow.this
                    , "请输入最大线程数量:", pro.getThreadNum());
            if (!StringTools.isEmptyWithTrimOrNull(numStr)) {
                try {
                    int num = Integer.parseInt(numStr);
                    pro.saveThreadNum(num);
                } catch (NumberFormatException e1) {
                    e1.printStackTrace();
                }
            }
        });
		menuItem_LikeType.addActionListener((e) -> {
			PropertiesTools pro = PropertiesTools.getInstance();
			String numStr = JOptionPane.showInputDialog(MainWindow.this
					, "请输入匹配方式(0 模糊匹配,1 左匹配,2 右匹配):", pro.getLikeType());
			if (!StringTools.isEmptyWithTrimOrNull(numStr)) {
				try {
					int num = Integer.parseInt(numStr);
					pro.saveLikeType(num);
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				}
			}
		});
        StartSearchListener startSearchListener = new StartSearchListener(this);
        btn_Start.addActionListener(startSearchListener);
        btn_Stop.addActionListener(new StopSearchListener(this, startSearchListener));
    }

    /**
     * 初始化 界面UI
     */
    private void initUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        int mainWindowW = 1116;
        int mainWindowH = 682;
        setBounds(100, 100, mainWindowW, mainWindowH);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        textField_SearchText = new JTextField();
        textField_SearchText.setBounds(20, 32, 846, 36);
        contentPane.add(textField_SearchText);
        textField_SearchText.setColumns(10);
        btn_Start = new JButton("开始搜索");
        btn_Start.setBounds(876, 31, 99, 36);
        contentPane.add(btn_Start);

        btn_Stop = new JButton("停止搜素");
        btn_Stop.setBounds(985, 31, 99, 36);
        btn_Stop.setEnabled(false);
        contentPane.add(btn_Stop);
        table = new JTable();
        int tableX = 0;
        int tableY = 80;
        int tableH = 540;
        table.setBounds(tableX, tableY, tableW, tableH);
        tableTitles = new String[]{columRowNumber, columTableName, columTableColumName, columSql};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{{"", "", "", ""}},
                tableTitles);
        table.setModel(model);
        /**
         * 初始化table 视图
         */
        // 创建表格标题对象
        JTableHeader head = table.getTableHeader();
        // 设置表头大小
        head.setPreferredSize(new Dimension(head.getWidth(), 35));
        // 设置表格字体
        head.setFont(new Font("", Font.PLAIN, 18));
        // 设置表格行宽
        table.setRowHeight(15);
        // 以下设置表格列宽
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setTableColumnSize();
        table.setAutoCreateRowSorter(true);
        table.setRowHeight(25);
        table.setCellSelectionEnabled(false);
        scrollPane_Table = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane_Table.setBounds(tableX, tableY, tableW + 10, tableH + 10);
        contentPane.add(scrollPane_Table);
        mainMeunBar = new JMenuBar();
        menu_Config = new JMenu("设置");
        mainMeunBar.setBounds(10, 0, 111, 22);


        menuItem_Db = new JMenuItem("数据库连接");
        menu_Config.add(menuItem_Db);

        menuItem_Thread = new JMenuItem("设置线程数");
        menu_Config.add(menuItem_Thread);

        menuItem_LikeType = new JMenuItem("匹配方式");
        menu_Config.add(menuItem_LikeType);

        menuItem_About = new JMenuItem("关于");
        menu_Config.add(menuItem_About);

        mainMeunBar.add(menu_Config);
        contentPane.add(mainMeunBar);
        setResizable(true);
    }

    public void setTableColumnSize() {
        table.getColumn(columRowNumber).setPreferredWidth((int) (tableW * 0.06));
        table.getColumn(columTableName).setPreferredWidth((int) (tableW * 0.2));
        table.getColumn(columTableColumName).setPreferredWidth((int) (tableW * 0.2));
        table.getColumn(columSql).setPreferredWidth((int) (tableW * 0.525));
    }


    @Override
    public JPanel getContentPane() {
        return contentPane;
    }

    public JTextField getTextField_SearchText() {
        return textField_SearchText;
    }

    public JTable getTable() {
        return table;
    }

    public JButton getBtn_Start() {
        return btn_Start;
    }

    public JButton getBtn_Stop() {
        return btn_Stop;
    }

    public JScrollPane getScrollPane_Table() {
        return scrollPane_Table;
    }

    public JMenu getMenu_Config() {
        return menu_Config;
    }

    public JMenuItem getMenuItem_Db() {
        return menuItem_Db;
    }

    public JMenuItem getMenuItem_Thread() {
        return menuItem_Thread;
    }

    public JMenuItem getMenuItem_About() {
        return menuItem_About;
    }

    public JMenuBar getMainMeunBar() {
        return mainMeunBar;
    }

    public String getColumTableName() {
        return columTableName;
    }

    public String getColumTableColumName() {
        return columTableColumName;
    }

    public String getColumSql() {
        return columSql;
    }

    public String getTitleStr() {
        return titleStr;
    }

    public String[] getTableTitles() {
        return tableTitles;
    }

    public String getColumRowNumber() {
        return columRowNumber;
    }
}
