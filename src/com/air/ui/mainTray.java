package com.air.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class mainTray {
	public void init() {
		SystemTray tray = SystemTray.getSystemTray();
		// 显示在托盘中的图标
		// 构造一个右键弹出式菜单
		PopupMenu pop = new PopupMenu();
		MenuItem traySH = new MenuItem("显示隐藏");
		MenuItem exit = new MenuItem("退出软件");
		MenuItem about = new MenuItem("关于(帮助)");
		pop.add(traySH);
		pop.add(exit);
		pop.add(about);
		traySH.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isv = !frmv.isVisible();
				logWindow.setLogWindowVisualble(isv);
				frmv.setVisible(isv);
			}
		});
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		about.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frmv, "版本 V1.0 build:2018年11月21日13:02:31", "关于:", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		TrayIcon trayIcon = new TrayIcon(new ImageIcon("images/tuopan.png").getImage(), "系统剪切板监听器V1.0\n版权所有：Air", pop);
		// 这句很重要，没有会导致图片显示不出来
		trayIcon.setImageAutoSize(true);
		trayIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					boolean isv = !frmv.isVisible();
					logWindow.setLogWindowVisualble(isv);
					frmv.setVisible(isv);
				}
			}
		});
		
		try {
			SystemTray.getSystemTray().add(trayIcon);
		} catch (AWTException e1) {
			e1.printStackTrace();
			logWindow.log(e1.getMessage());
		}
	}

	public mainTray(MainWindow window, JFrame frmv) {
		super();
		this.window = window;
		this.frmv = frmv;
		init();
	}

	private MainWindow window = null;
	private JFrame frmv = null;
}
