package com.air.beans;

/**
 * 搜索结果bean
 * @author Air
 * @since v1
 * @date 2018年11月21日14:07:01
 */
public class SearchResult {
    private String tableName;
    private String columName;
    private String sql;

    @Override
    public String toString() {
        return "SearchResult{" +
                "tableName='" + tableName + '\'' +
                ", columName='" + columName + '\'' +
                ", sql='" + sql + '\'' +
                '}';
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumName() {
        return columName;
    }

    public void setColumName(String columName) {
        this.columName = columName;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
