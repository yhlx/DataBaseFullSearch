package com.air.tools;

/**
 * 字符串工具类
 * @author Air
 * @since  v1
 * @date 2018年11月21日11:40:03
 */
public class StringTools {
    /***
     * 字符串是否是null或者trim后为空
     * @param str
     * @return true 是, false 有非空内容
     */
    public static final boolean isEmptyWithTrimOrNull(String str){
        return null == str || str.trim().isEmpty();
    }

    public static String EMTPYSTRING = "";
}
