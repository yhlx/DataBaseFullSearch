package com.air.tools;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * 封装 配置文件的读写
 *
 * @author Air
 * @date 2018年11月21日10:59:17
 * @since v1
 */
public class PropertiesTools {
    /**
     * 获取数据库地址
     *
     * @return if 没有就""
     */
    public String getDbUrl() {
        return getConfValueByKey(CONF_DBURL);
    }

    /**
     * 保存数据库地址
     *
     * @param value
     */
    public void saveDbUrl(String value) {
        setConfValueByKey(CONF_DBURL, value);
    }

    /**
     * 获取数据库类型支持列表
     *
     * @return if 没有就""
     */
    public List<String> getDbTypeList() {
        ArrayList<String> re = new ArrayList<>(5);
        String str = getConfValueByKey(CONF_DBTYPELIST);
        if (!str.isEmpty()) {
            re.addAll(Arrays.asList(str.split(",")));
        }
        return re;
    }

    /**
     * 是否快速模式
     *
     * @return if 没有就""
     */
    public boolean isFastModel() {
        String fast = getConfValueByKey(CONF_FAST);
        return "true".equals(fast);
    }

    /**
     * 获取数据库类型
     *
     * @return if 没有就""
     */
    public int getDbType() {
        int re = 0;
        String confValueByKey = getConfValueByKey(CONF_DBTYPE);
        if (!StringTools.isEmptyWithTrimOrNull(confValueByKey)) {
            try {
                re = Integer.parseInt(confValueByKey);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return re;
    }

    /**
     * 保存数据库类型
     *
     * @param value
     */
    public void saveDbType(String value) {
        setConfValueByKey(CONF_DBTYPE, value);
    }

    /**
     * 获取数据库用户名
     *
     * @return if 没有就""
     */
    public String getDbUser() {
        return getConfValueByKey(CONF_DBUSER);
    }

    /**
     * 保存数据库用户名
     *
     * @param value
     */
    public void saveDbUser(String value) {
        setConfValueByKey(CONF_DBUSER, value);
    }

    /**
     * 获取数据库密码
     *
     * @return if 没有就""
     */
    public String getDbPass() {
        return getConfValueByKey(CONF_DBPASS);
    }

    /**
     * 保存数据库密码
     *
     * @param value
     */
    public void saveDbPass(String value) {
        setConfValueByKey(CONF_DBPASS, value);
    }

    /**
     * 获取配置的最多线程数量
     *
     * @return if 没有就  50
     */
    public int getThreadNum() {
        int num = 50;
        String numStr = getConfValueByKey(CONF_THREADNUMS);
        if (!numStr.isEmpty()) {
            try {
                num = Integer.parseInt(numStr);
            } catch (NumberFormatException e) {
            }
        }
        return num;
    }

    /**
     * @return if 没有就  0
     */
    public int getLikeType() {
        int num = 0;
        String numStr = getConfValueByKey(CONF_LIKETYPE);
        if (!numStr.isEmpty()) {
            try {
                num = Integer.parseInt(numStr);
            } catch (NumberFormatException e) {
            }
        }
        return num;
    }

    /**
     * 保存最大线程数到配置
     *
     * @param num
     */
    public void saveThreadNum(int num) {
        setConfValueByKey(CONF_THREADNUMS, String.valueOf(num));
    }

    /**
     * 匹配方式(0 模糊匹配,1 左匹配,2 右匹配)
     *
     * @param num
     */
    public void saveLikeType(int type) {
        setConfValueByKey(CONF_LIKETYPE, String.valueOf(type));
    }

    /**
     * 获取数据库类型对应的jdbc类
     *
     * @param dbType 数据库类型 也就是选择的第几个数据库
     * @return 如果没有返回 ""
     */
    public String getJdbcClass(int dbType) {
        String re = StringTools.EMTPYSTRING;
        String str = getConfValueByKey(CONF_CLASS);
        if (!StringTools.isEmptyWithTrimOrNull(str)) {
            String[] cs = str.split(",");
            re = cs[dbType];
        }
        return re;
    }

    /**
     * 获取指定数据库类型的获取全部表名列表的sql语句
     *
     * @return if 没有就""
     */
    public String getSqlQueryAllTableNames(int dbtype) {
        String re = getValueFormProperties(SQLCONF_ALLTABLE + dbtype, properties_QueryTableSql);
        re = StringTools.isEmptyWithTrimOrNull(re) ? StringTools.EMTPYSTRING : re;
        return re;
    }

    /**
     * 获取conf.ini的对应key的值
     *
     * @param key key
     * @return key的值or "" if 没有值或key
     */
    public String getConfValueByKey(String key) {
        return getValueFormProperties(key, properties_Conf);
    }

    /**
     * 设置conf.ini的key的值 key没有就新增到文件
     *
     * @param key
     * @param value
     */
    public void setConfValueByKey(String key, String value) {
        setValueToPropertiesFile(key, value
                , properties_Conf, File_Conf);
    }

    public static PropertiesTools getInstance() {
        if (null == propertiesTools) {
            synchronized (lock) {
                if (null == propertiesTools) {
                    propertiesTools = new PropertiesTools();
                }
            }
        }
        return propertiesTools;
    }

    private PropertiesTools() {
        try {
            properties_Conf = new Properties();
            properties_Conf.load(new FileReader(File_Conf));
            properties_QueryTableSql = new Properties();
            properties_QueryTableSql.load(new FileReader(File_QueryTableSql));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setValueToPropertiesFile(String key, String value, Properties pro, File file) {
        try {
            pro.setProperty(key, value);
            pro.store(new FileWriter(file), "update config file");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getValueFormProperties(String key, Properties pro) {
        String property = null;
        try {
            property = pro.getProperty(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null == property ? "" : property;
    }

    // conf.ini
    private static Properties properties_Conf = null;
    private static File File_Conf = new File("conf.ini");
    // querytablesql.ini
    private static Properties properties_QueryTableSql = null;
    private static File File_QueryTableSql = new File("querytablesql.ini");
    public static final String CONF_DBTYPE = "dbtype";
    public static final String CONF_FAST = "fast";
    public static final String CONF_DBTYPELIST = "datypelist";
    public static final String CONF_DBURL = "dburl";
    public static final String CONF_DBUSER = "dbuser";
    public static final String CONF_DBPASS = "dbpass";
    public static final String CONF_THREADNUMS = "threadNums";
    public static final String CONF_LIKETYPE = "like_type";
    public static final String CONF_CLASS = "class";
    public static final String SQLCONF_ALLTABLE = "queryalltab";
    public static final String SQLCONF_LIMITSQL = "limitsql";
    private static final Object lock = new Object();
    private static PropertiesTools propertiesTools = null;


}
