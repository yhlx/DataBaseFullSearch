package com.air.tools;

import com.air.ui.logWindow;

/**
 * 负责 调试信息输出
 * @author Administrator
 *
 */
public class output {
	/**\
	 * 输出 一条调试信息 并且换行
	 * @param s
	 */
	public static void outLine(String s){
		if(isdebug) {
			logWindow.log(s);
		}
	}
	/**\
	 *  调试信息  换行
	 * @param  
	 */
	public static void outLine( ){
		if(isdebug) {
			logWindow.log("");
		}
	}
	/**
	 * 设置 是否 调试模式, 是 输出 调试信息, 否 不输出
	 * @param b
	 */
	public static void setDevModel(boolean b) {
		isdebug = b;
	}
	private static boolean isdebug = true;
}
