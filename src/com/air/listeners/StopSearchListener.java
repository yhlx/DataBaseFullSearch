package com.air.listeners;

import com.air.ui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 停止搜素按钮 监听
 */
public class StopSearchListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        s.stop();
        s.allFinalsh("   搜索终止!");
    }

    public StopSearchListener(MainWindow mw, StartSearchListener startSearchListener) {
        this.mw = mw;
        this.s = startSearchListener;
    }

    private MainWindow mw ;
    private StartSearchListener s ;
}
